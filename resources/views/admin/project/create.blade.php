@extends('admin/layouts/master')

@section('content')
    <main class="app-content">
<div class="card">
    <div class="card-header bg-success">
        <div class="float-left text-white">
            Create Project
        </div>
        <div class="float-right">
            <a href="{{ url('/project') }}" class="btn btn-primary">Project List</a>
        </div>
    </div>
    <div class="card-body">

        @if ($errors->any())
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['url' => 'project','enctype'=>'multipart/form-data']) !!}
            @include('admin.project.form')
            <div class="form-group row">
                <div class="col-sm-10 text-center">
                    {!! Form::button('Add', [
                                                'class' => 'btn btn-primary',
                                                'type' => 'submit',
                                            ]) !!}
                </div>
            </div>
        {!! Form::close() !!}
        {{--</form>--}}
    </div>
</div>
    <main>
@endsection
