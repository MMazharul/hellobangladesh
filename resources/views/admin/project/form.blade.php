<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Title', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, [
                'placeholder' => 'Title',
                'class' => 'form-control',
                'id' => 'Title',
            ]) !!}<br>
        @if ($errors->has('title'))
            {{ $errors->first('title') }}
        @endif
    </div>
</div>

<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Picture', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::file('picture', null, [
                'placeholder' => '',
                'class' => 'form-control',
                'id' => 'Title',
            ]) !!}<br>
        @if ($errors->has('picture'))
            {{ $errors->first('picture') }}
        @endif
    </div>

</div>
@if(isset($project->picture))
<div class="form-group row">
    {!! Form::label('', null, ['class' => 'col-sm-2 col-form-label']) !!}
    <div class="col-sm-10">
        <img src="{{asset('ui/backend/images/'.$project->picture)}}" width="200px" height="150px">
    </div>
</div>
@endif



<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Short Description', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('short_description', null, [
                'placeholder' => 'Short Description....',
                'class' => 'form-control',
                'id' => 'short_description',
                'cols' => 5,
                'rows' => 5,
            ]) !!}<br>
        @if ($errors->has('short_description'))
            {{ $errors->first('short_description') }}
        @endif
    </div>
</div>


<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Long Description', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('long_description', null, [
                'placeholder' => 'Long Description....',
                'class' => 'form-control',
                'id' => 'long_description',
                'cols' => 5,
                'rows' => 5,
            ]) !!}<br>
        @if ($errors->has('long_description'))
            {{ $errors->first('long_description') }}
        @endif
    </div>
</div>

<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Raised Amount', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::number('raised_amount', null, [
                'placeholder' => 'Raised Amount',
                'class' => 'form-control',
                'id' => 'raised_amount',
            ]) !!}<br>
        @if ($errors->has('raised_amount'))
            {{ $errors->first('raised_amount') }}
        @endif
    </div>
</div>

<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Goal Amount', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::number('goal_amount', null, [
                'placeholder' => 'Goal Amount',
                'class' => 'form-control',
                'id' => 'goal_amount',
            ]) !!}<br>
        @if ($errors->has('goal_amount'))
            {{ $errors->first('goal_amount') }}
        @endif
    </div>
</div>

<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Status', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {{ Form::radio('status', 1,[
          'class' => 'form-control',
        ]) }} Active
        {{ Form::radio('status', 0,
        [
          'class' => 'form-control',
        ]) }} Diactive
    </div>
</div>