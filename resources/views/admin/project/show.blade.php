<div style="margin:0 auto; width: 500px;">
    <a href="{{ url('/directories') }}">List</a>
    <table border="1" cellpadding="5" cellspacing="5" width="100%">
        <caption>Directory Details</caption>
        <tr>
            <td>Title</td>
            <td>{{ $directory->title }}</td>
        </tr>
        <tr>
            <td>Files</td>
            <td>
                <ul>
                @foreach($directory->files as $file)
                    <li><a href="#">{{ $file->title }}</a></li>
                 @endforeach
                </ul>
            </td>
        </tr>
        <tr>
            <td>Created At</td>
            <td>{{ $directory->created_at }}</td>
        </tr>
    </table>
</div>
