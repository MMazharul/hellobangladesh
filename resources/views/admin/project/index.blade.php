@extends('admin.layouts.master')

@section('content')
<main class="app-content">
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <h4><a style="text-decoration: none" href="{{url('/project/create')}}"><i class="fa fa-plus-square"></i> Project</a></h4>

                    <table class="table table-hover table-bordered" id="sampleTable">



                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Picture</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                            <tr>
                                <td>{{$project->title}}</td>
                                <td><img src="{{asset('/ui/backend/images/'.$project->picture)}}" width="100" height="80"></td>
                                <td>{{$project->created_at	}}</td>
                                <td>{{$project->updated_at	}}</td>
                                <td><a href="{{url('/project/'.$project->id.'/edit')}}"><i class=" fa fa-lg fa-edit"></i> </a></td>
                                <td><a href="{{ url('project/'.$project->id)}}" onclick="event.preventDefault();
                                                     document.getElementById('button').submit();">
                                        <i class="fa fa-lg fa-trash"></i> </a>
                                </td>


                                <form id="button" action="{{url('project', [$project->id])}}" method="POST" style="display: none">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                </form>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </main>
@endsection
