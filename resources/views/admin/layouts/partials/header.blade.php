<header class="app-header"><a class="app-header__logo" href="index.html">Hello Bangladesh</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <li class="app-search">
            <input class="app-search__input" type="search" placeholder="Search">
            <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!--Notification Menu-->

        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                <li>
                    {{--<a class="dropdown-item" href="page-login.html"><i class="fa fa-sign-out fa-lg"></i> Logout</a>--}}

                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out fa-lg"></i>{{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </li>
            </ul>
        </li>
    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="{{asset('/ui/backend/image/volunteers/'.\Illuminate\Support\Facades\Auth::user()->picture)}}" alt="User Image">
        <div>
            <p class="app-sidebar__user-name">{{\Illuminate\Support\Facades\Auth::user()->name}}</p>
            <p class="app-sidebar__user-designation">Admin</p>
        </div>
    </div>
    <ul class="app-menu">
        <li><a class="app-menu__item active" href="index.html"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>

        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">Project</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{url('project/create')}}"><i class="icon fa fa-circle-o"></i>Create</a></li>
                <li><a class="treeview-item" href="{{url('/project')}}"  rel="noopener"><i class="icon fa fa-circle-o"></i>Projects</a></li>

            </ul>
        </li>

        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-laptop"></i><span class="app-menu__label">Events</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{url('/event/create')}}"><i class="icon fa fa-circle-o"></i>Create</a></li>
                <li><a class="treeview-item" href="{{url('/event')}}"  rel="noopener"><i class="icon fa fa-circle-o"></i>Events</a></li>

            </ul>
        </li>

        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-sliders"></i><span class="app-menu__label">Sliders</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{url('/slider/create')}}"><i class="icon fa fa-circle-o"></i>Create</a></li>
                <li><a class="treeview-item" href="{{url('/slider')}}"><i class="icon fa fa-circle-o"></i>Sliders</a></li>

            </ul>
        </li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Volunteer</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="table-basic.html"><i class="icon fa fa-circle-o"></i>add</a></li>
                <li><a class="treeview-item" href="{{url('/volunteer')}}"><i class="icon fa fa-circle-o"></i>Volunteers</a></li>
            </ul>
        </li>

        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Sponsor</span><i class="treeview-indicator fa fa-angle-right"></i></a>
            <ul class="treeview-menu">
                <li><a class="treeview-item" href="{{url('/sponsor/create')}}"><i class="icon fa fa-circle-o"></i>add</a></li>
                <li><a class="treeview-item" href="{{url('/sponsor')}}"><i class="icon fa fa-circle-o"></i>sponsors</a></li>
            </ul>
        </li>

        <li class="treeview"><a class="app-menu__item" href="{{url('/donation')}}" ><i class="app-menu__icon fa fa-dollar"></i><span class="app-menu__label">Donator</span><i class="treeview-indicator fa fa-angle-right"></i></a>

        </li>

    </ul>
</aside>