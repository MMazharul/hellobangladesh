@extends('admin.layouts.master')

@section('content')
<main class="app-content">
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <h4><a style="text-decoration: none" href="{{url('/slider/create')}}"><i class="fa fa-plus-square"></i> slider</a></h4>

                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Picture</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sliders as $slider)
                            <tr>
                                <td>{{$slider->title}}</td>
                                <td><img src="{{asset('/ui/backend/images/'.$slider->picture)}}" width="100px" height="80px"></td>
                                <td>{{$slider->created_at	}}</td>
                                <td>{{$slider->updated_at	}}</td>
                                <td><a href="{{url('/slider/'.$slider->id.'/edit')}}"><i class=" fa fa-lg fa-edit"></i> </a></td>
                                <td><a href="{{url('/slider/'.$slider->id.'/delete')}}" ><i class="fa fa-lg fa-trash"></i> </a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </main>
@endsection
