<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Title', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, [
                'placeholder' => 'Title',
                'class' => 'form-control',
                'id' => 'Title',
            ]) !!}<br>
        @if ($errors->has('title'))
            {{ $errors->first('title') }}
        @endif
    </div>
</div>


<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Short Description', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('short_description', null, [
                'placeholder' => 'Short Description',
                'class' => 'form-control',
                'id' => 'short_description',
            ]) !!}<br>
        @if ($errors->has('short_description'))
            {{ $errors->first('short_description') }}
        @endif
    </div>
</div>


<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('picture', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::file('picture', [
                'placeholder' => 'Picture',
                'class' => 'form-control',
                'id' => 'picture',
            ]) !!}<br>
        @if ($errors->has('picture'))
            {{ $errors->first('picture') }}
        @endif
    </div>
</div>

@if(isset($slider->picture))
    <div class="form-group row">
        {!! Form::label('', null, ['class' => 'col-sm-2 col-form-label']) !!}
        <div class="col-sm-10">
            <img src="{{asset('ui/backend/images/'.$slider->picture)}}" width="200px" height="150px">
        </div>
    </div>
@endif



