@extends('admin.layouts.master')

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <div class="tile-body">

                        <h4><a style="text-decoration: none" href="{{url('/event/create')}}"><i class="fa fa-plus-square"></i> event</a></h4>

                        <table class="table table-hover table-bordered" id="sampleTable">
                            <thead>
                            <tr>
                                <th>Project</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Mobile</th>
                                <th>Amount</th>
                                <th>Transaction</th>
                                <th>Message</th>
                                <th>Aprove</th>
                                <th>Create_at</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($donations as $donation)
                                <tr>
                                    <td>{{$donation->project}}</td>
                                    <td>{{$donation->firstname}}</td>
                                    <td>{{$donation->lastname}}</td>
                                    <td>{{$donation->mobile}}</td>
                                    <td>{{$donation->amount	}}</td>
                                    <td>{{$donation->transaction}}</td>
                                    <td>{{$donation->message}}</td>
                                    <td>Approve</td>
                                    <td>{{$donation->created_at}}</td>
                                    <td><a href="{{url('/event/'.$donation->id.'/delete')}}" ><i class="fa fa-lg fa-trash"></i> </a></td>


                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
