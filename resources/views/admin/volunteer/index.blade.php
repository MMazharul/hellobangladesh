@extends('admin.layouts.master')

@section('content')
<main class="app-content">
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">
                    

                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Photo</th>
                            <th>Date of Birth</th>
                            <th>Gender</th>
                            <th>Address</th>
                            <th>Contact Number</th>
                            <th>Blood Group</th>
                            <th>Profession</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($volunteers as $volunteer)
                            <tr>
                            <td>{{$volunteer->first_name}}</td>
                            <td>{{$volunteer->last_name}}</td>
                            <td><img src="{{asset('/ui/backend/images/'.$volunteer->picture)}}"></td>
                            <td>{{$volunteer->date_of_birth}}</td>
                            <td>{{$volunteer->gender}}</td>
                            <td>{{$volunteer->address}}</td>
                            <td>{{$volunteer->phone}}</td>
                            <td>{{$volunteer->blood_group}}</td>
                            <td>{{$volunteer->profession}}</td>
                            <td><a href="{{url('/volunteer/'.$volunteer->id.'/edit')}}" ><i class="fa fa-lg fa-edit"></i> </a></td>
                            <td><a href="{{ url('volunteer/'.$volunteer->id)}}" onclick="event.preventDefault();
                                                     document.getElementById('button').submit();">
                                    <i class="fa fa-lg fa-trash"></i> </a>
                            </td>


                                <form id="button" action="{{url('volunteer', [$volunteer->id])}}" method="POST" style="display: none">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                </form>


                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
