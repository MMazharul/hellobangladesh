<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('FirstName', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('firstname', null, [
                'placeholder' => 'FirstName',
                'class' => 'form-control',
                'id' => 'firstname',
            ]) !!}<br>
        @if ($errors->has('firstname'))
            {{ $errors->first('firstname') }}
        @endif
    </div>
</div>

<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('FirstName', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('firstname', null, [
                'placeholder' => 'FirstName',
                'class' => 'form-control',
                'id' => 'firstname',
            ]) !!}<br>
        @if ($errors->has('firstname'))
            {{ $errors->first('firstname') }}
        @endif
    </div>
</div>

<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('picture', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::file('picture', [
                'placeholder' => 'Picture',
                'class' => 'form-control',
                'id' => 'picture',
            ]) !!}<br>
        @if ($errors->has('picture'))
            {{ $errors->first('picture') }}
        @endif
    </div>
</div>


<div class="form-group row">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Profession', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('profession', null, [
                'placeholder' => 'Profession',
                'class' => 'form-control',
                'id' => 'profession',
            ]) !!}<br>
        @if ($errors->has('profession'))
            {{ $errors->first('profession') }}
        @endif
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-10">
        Workdays
        {!! Form::label('Profession') !!}
        {!! Form::checkbox('Student', 'Job holder') !!}
    </div>
</div>

