@extends('admin/layouts/master')

@section('content')
    <style>
        #custom{
            margin-left:80px;
        }
    </style>

    <main class="app-content">
        <div class="row">
            <div class="col-md-10" id="custom">
                <div class="tile">
                    <h3 class="tile-title">Event Edit</h3>
                    <div class="tile-body">

            @if ($errors->any())
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            {!! Form::model($event,[
                        'url' => ['event', $event->id],
                        'method' => 'put'
                        ]) !!}
                @include('admin.event.form')


                <div class="tile-footer">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="{{url('/event')}}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                        </div>
                    </div>
                </div>

            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
