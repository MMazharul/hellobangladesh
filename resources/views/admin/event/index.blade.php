@extends('admin.layouts.master')

@section('content')
<main class="app-content">
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <h4><a style="text-decoration: none" href="{{url('/event/create')}}"><i class="fa fa-plus-square"></i> event</a></h4>

                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>Project Name</th>
                            <th>Event Title</th>
                            <th>Picture</th>
                            <th>detail</th>
                            <th>Programme Date</th>
                            <th>location</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($events as $event)
                            <tr>
                            <td>{{$event->project_name}}</td>
                            <td>{{$event->title}}</td>
                            <td><img src="{{asset('//ui/backend/images/'.$event->picture)}}" width="100px" height="80px"></td>
                            <td>{{$event->detail}}</td>
                            <td>{{$event->date}}</td>
                            <td>{{$event->location}}</td>
                            @if($event->status==1)
                                    <td>Programme Completed</td>
                             @else
                                    <td>Programme Incomplited</td>
                             @endif
                            <td><a href="{{url('/event/'.$event->id.'/edit')}}"><i class=" fa fa-lg fa-edit"></i> </a></td>
                            <td><a href="{{url('/event/'.$event->id.'/delete')}}" ><i class="fa fa-lg fa-trash"></i> </a></td>


                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
