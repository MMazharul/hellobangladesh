<div class="form-group">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Project Select', null, ['class' => 'control-label']) !!}

        <select name="project_name" class="form-control">
            <option disabled="true" selected="true">Select Your Project</option>
            @foreach($projects as $project)
                <option value="{{$project}}">{{$project}}</option>
            @endforeach
        </select>

        {{--@if ($errors->has('title'))--}}
            {{--{{ $errors->first('title') }}--}}
        {{--@endif--}}
</div>

<div class="form-group">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Title', null, ['class' => 'control-label']) !!}


    {!! Form::text('title', null, [
            'placeholder' => 'Title....',
            'class' => 'form-control',
            'id' => 'Title',
        ]) !!}<br>
    {{--@if ($errors->has('title'))--}}
    {{--{{ $errors->first('title') }}--}}
    {{--@endif--}}
</div>

<div class="form-group">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Facebook Link', null, ['class' => 'control-label']) !!}


    {!! Form::text('facebook_link', null, [
            'placeholder' => 'Facebook Link....',
            'class' => 'form-control',
            'id' => 'facebook_link',
        ]) !!}<br>
    {{--@if ($errors->has('title'))--}}
    {{--{{ $errors->first('title') }}--}}
    {{--@endif--}}
</div>


<div class="form-group">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Event Details', null, ['class' => 'control-label']) !!}


    {!! Form::textarea('detail', null, [
            'placeholder' => 'Event Details.....',
            'class' => 'form-control',
            'id' => 'Title',
            'rows'=>4
        ]) !!}<br>
    {{--@if ($errors->has('title'))--}}
    {{--{{ $errors->first('title') }}--}}
    {{--@endif--}}
</div>

<div class="form-group">
    {{--<input class="form-control" id="demoDate" type="text" placeholder="">--}}

    {!! Form::label('Date', null, ['class' => 'control-label']) !!}


    {!! Form::date('date', null, [
            'placeholder' => 'Select Date',
            'class' => 'form-control',
            'id' => 'demoDate',

        ]) !!}<br>
</div>

<div class="form-group">


    {!! Form::label('Location', null, ['class' => 'control-label']) !!}


    {!! Form::text('location', null, [
            'placeholder' => 'Location...',
            'class' => 'form-control',
            'id' => 'location',

        ]) !!}<br>
</div>


<div class="form-group">
    {{--<label for="title" class="col-sm-2 col-form-label">Title</label>--}}
    {!! Form::label('Project Status', null, ['class' => 'col-sm-2 col-form-label']) !!}

    <div class="col-sm-10">
        {{ Form::radio('status', 1,[
          'class' => 'form-control',
        ]) }} Complite
        {{ Form::radio('status', 0,
        [
          'class' => 'form-control',
        ]) }} Incomplite
    </div>
</div>


