@extends('admin/layouts/master')

@section('content')
    <style>
        #custom{
            margin-left:80px;
        }
    </style>

    <main class="app-content">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <div class="row">
        <div class="col-md-10" id="custom">
            <div class="tile">
                <h3 class="tile-title">Event Create</h3>
                <div class="tile-body">
                    {!! Form::open(['url' => '/event']) !!}
                    @include('admin.event.form')
                    {{--<div class="form-group row">--}}
                        {{--<div class="col-sm-10 text-center">--}}
                            {{--{!! Form::button('Add', [--}}
                                                        {{--'class' => 'btn btn-primary',--}}
                                                        {{--'type' => 'submit',--}}
                                                    {{--]) !!}--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="tile-footer">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-3">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="{{url('/event')}}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </main>

@endsection
