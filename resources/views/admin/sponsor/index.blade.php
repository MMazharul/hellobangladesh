@extends('admin.layouts.master')

@section('content')
<main class="app-content">
    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="tile-body">

                    <h4><a style="text-decoration: none" href="{{url('/sponsor/create')}}"><i class="fa fa-plus-square"></i> Sponsor</a></h4>

                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Picture</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sponsors as $sponsor)
                            <tr>

                                <td>{{$sponsor->title}}</td>
                                <td><img src="{{asset('/ui/backend/images/'.$sponsor->picture)}}"></td>
                                <td>{{$sponsor->created_at	}}</td>
                                <td>{{$sponsor->updated_at	}}</td>
                                <td><a href="{{url('/sponsor/'.$sponsor->id.'/edit')}}"><i class=" fa fa-lg fa-edit"></i> </a></td>


                                <td><a href="{{ url('sponsor/'.$sponsor->id)}}" onclick="event.preventDefault();
                                                     document.getElementById('button').submit();">
                                        <i class="fa fa-lg fa-trash"></i> </a>
                                </td>


                                <form id="button" action="{{url('sponsor', [$sponsor->id])}}" method="POST" style="display: none">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                </form>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </main>
@endsection
