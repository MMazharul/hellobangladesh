<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Hello | Bangladesh</title>

    <!-- Icons font CSS-->
    <link href="{{asset('/ui/frontend/form/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('ui/frontend/form/main.css')}}" rel="stylesheet" media="all">




    <style>
       h3{
           font-size: 24px;
           text-align: center;
           margin-top: 21px;
           margin-bottom: -73px;
           color:lightseagreen;
       }

    </style>

</head>

<body>

@if(session('msg'))
    <div class="alert alert-success alert-dismissible" role="alert" id="message">
        <div class="icon"><span class="mdi mdi-check"></span></div>
        <div class="message text-center "><h3>{{ session('msg') }}</h3></div>
    </div>
@endif
<div class="page-wrapper  p-t-100 p-b-100 font-robo">


    <div class="wrapper wrapper--w680">

        <div class="card card-1">

            <div class="card-heading"></div>
            <div class="card-body">
                <h2 class="title">Hello Bangladesh Registration Info</h2>
                <form method="POST" action="{{url('/volunteer')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="First Name" name="first_name">
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-1" type="text" placeholder="Last Name" name="last_name">
                            </div>
                        </div>

                    </div>
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-1 js-datepicker" type="text" placeholder="BIRTHDATE" name="date_of_birth">
                                <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <div class="rs-select2 js-select-simple select--no-search">
                                    <select name="gender">
                                        <option disabled="disabled" selected="selected">GENDER</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                        <option>Other</option>
                                    </select>
                                    <div class="select-dropdown"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="input-group">
                        <input class="input--style-1" type="text" placeholder="Address" name="address">

                    </div>


                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <input class="input--style-1" type="number" placeholder="Contact Number" name="phone">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <div class="rs-select2 js-select-simple select--no-search">
                                    <select name="blood_group">
                                        <option disabled="disabled" selected="selected">Blood Group</option>
                                        <option>A+</option>
                                        <option>B+</option>
                                        <option>AB+</option>
                                    </select>
                                    <div class="select-dropdown"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-2">
                            <label>Your Picture Upload</label><br>
                            <div class="input-group"><br>
                                <input class="" type="file" placeholder="Your Picture Upload" name="picture">
                            </div>
                        </div>

                            <div class="col-2">
                                <div class="input-group"><br>
                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <select name="profession">
                                            <option disabled="disabled" selected="selected">Profession</option>
                                            <option>Student</option>
                                            <option>Job Holder</option>
                                            <option>Bekar</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="p-t-20">
                        <button class="btn btn--radius btn--green" type="submit">Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script>
    jQuery(
        function($) {
            $('#message').fadeIn (5050);
            $('#message').fadeOut (5050);

        }
    )

</script>

<!-- Jquery JS-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Vendor JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.js"></script>


<!-- Main JS-->
<script src="{{asset('/ui/frontend/form/global.js')}}"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
