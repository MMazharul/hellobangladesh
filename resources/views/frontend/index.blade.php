@extends('frontend.layout.master-layout')
@section('main_content')



    <section class="main-slider-two">

        <div class="main-slider-carousel owl-carousel owl-theme">

            @foreach($sliders as $slider)

                <div class="slide" style="background-image:url({{asset('/ui/backend/images/'.$slider->picture)}})">
                    <div class="auto-container">
                        <div class="content text-center">
                            <div class="title">{{$slider->title}}</div>
                            <h2>{{$slider->short_description}}</h2>
                            <div class="donate-percentage">
                                <div class="donate-bar">
                                    <div class="bar-inner"><div class="bar progress-line" data-width="75"><div class="count-box"><span class="count-text" data-speed="2500" data-stop="75">0</span>%</div></div></div>
                                </div>
                                <div class="donate-bar-info">
                                    <div class="donate-percent"></div>
                                </div>
                                <div class="amounts clearfix">
                                    <div class="pull-left number">Raised: $7,500</div>
                                    <div class="pull-right number">Goal: $10,000</div>
                                </div>
                            </div>
                            <div class="link-box">
                                <div class="text-center clearfix">
                                    <a href="{{url('/donation/create')}}" target="_blank" class="theme-btn btn-style-five">Donate Now</a>
                                    <a href="about.html" class="theme-btn btn-style-six"><span class="txt">learn more</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

        </div>

        <!--Scroll Dwwn Btn-->
        <div class="mouse-btn-down scroll-to-target" data-target=".welcome-section"></div>
    </section>
    <!--End Main Slider-->

    <!-- Urgent Causes Section -->
    <section class="urgent-causes-section" style="background-image:url(ui/frontend/images/background/4.jpg)">
        <div class="auto-container">
            <div class="row clearfix">

                <!-- Title Column -->
                <div class="title-column col-lg-5 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <!-- Sec Title -->
                        <div class="sec-title">
                            <h2><span class="theme_color">Most Urgent </span> Cause</h2>
                        </div>
                        <div class="bold-text">Donate Now Feed a Kashmir Children</div>
                        <div class="text">We & our scholars appreciate your support for the cause of eduction in Kashmir. Give online by useing the donate buttons below.</div>
                        <a href="https://donate.jaago.com.bd/" class="theme-btn btn-style-five">join us</a>
                        <a href="about.html" class="theme-btn btn-style-two">Learn more</a>
                    </div>
                </div>

                <!-- Donate Column -->
                <div class="donate-column col-lg-7 col-md-12 col-sm-12">
                    <div class="inner-column">

                        <div class="row clearfix">
                            <div class="column col-lg-3 col-md-3 col-sm-12">
                                <div class="title">Complete</div>
                                <h3>68%</h3>
                            </div>
                            <div class="column col-lg-3 col-md-3 col-sm-12">
                                <div class="title">Collected</div>
                                <h3>$ 36, 800</h3>
                            </div>
                            <div class="column col-lg-3 col-md-3 col-sm-12">
                                <div class="title">Donateors</div>
                                <h3>170</h3>
                            </div>
                            <div class="column col-lg-3 col-md-3 col-sm-12">
                                <div class="title">Needed</div>
                                <h3>$ 27,600</h3>
                            </div>
                        </div>

                        <!-- Donate Percentage -->
                        <div class="donate-percentage">
                            <div class="donate-bar">
                                <div class="bar-inner"><div class="bar progress-line" data-width="75"><div class="count-box"><span class="count-text" data-speed="2500" data-stop="75">0</span>%</div></div></div>
                            </div>
                            <div class="donate-bar-info">
                                <div class="donate-percent"></div>
                            </div>
                            <div class="amounts clearfix">
                                <div class="pull-left number">$ 1,27,600 <span>Collected</span></div>
                                <div class="pull-right"><a href="{{url('/donation/create')}}" target="_blank" class="theme-btn btn-style-five">Donate Now</a></div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Urgent Causes Section -->

    <!-- Services Section -->
    <section class="services-section-two" id="mission">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title centered">
                <h2><span class="theme_color">Mission & </span> Goals</h2>
                <div class="text">Many children and poor people are at high risk <br> of severe malnutrition</div>
            </div>
            <div class="row clearfix">

                <!-- Service Block Two -->
                <div class="service-block-two col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-money-bag"></span>
                        </div>
                        <h3><a href="causes-single.html">Make a Donation</a></h3>
                        <div class="text">Default text for Box Content. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                    </div>
                </div>

                <!-- Service Block Two -->
                <div class="service-block-two col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-donation-2"></span>
                        </div>
                        <h3><a href="causes-single.html">Become A Volunteer</a></h3>
                        <div class="text">Default text for Box Content. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                    </div>
                </div>

                <!-- Service Block Two -->
                <div class="service-block-two col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-house-1"></span>
                        </div>
                        <h3><a href="causes-single.html">Shelter for Homeless</a></h3>
                        <div class="text">Default text for Box Content. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                    </div>
                </div>

                <!-- Service Block Two -->
                <div class="service-block-two col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-box wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-world"></span>
                        </div>
                        <h3><a href="causes-single.html">Make World Happier</a></h3>
                        <div class="text">Default text for Box Content. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="popular-causes-section" style="background-image:url(ui/frontend/images/background/5.jpg)">
        <div class="auto-container">
            <div class="row clearfix">



                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            @if(isset($project->picture))
                            <img src="{{asset('ui/backend/images/'.$project->picture)}}" alt="" />
                            @endif
                        </div>
                    </div>
                </div>

                <!-- Content Column -->
                <div class="content-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <!-- Sec Title -->
                        <div class="sec-title light">
                            @if(isset($project->title))
                            <h2><span class="theme_color">{{$project->title}}</span></h2>
                             @endif
                        </div>
                        <div class="bold-text"> @if(isset($project->short_description)){{$project->short_description}}@endif</div>
                        <div class="text">@if(isset($project->long_description)){{$project->long_description}}@endif</div>
                        <div class="donate-percentage">
                            <div class="donate-bar">
                                <div class="bar-inner"><div class="bar progress-line" data-width="@if(isset($project->raised_amount)){{100*$project->raised_amount/$project->goal_amount}}@endif"><div class="count-box"><span class="count-text" data-speed="2500" data-stop="@if(isset($project->raised_amount)){{100*$project->raised_amount/$project->goal_amount}}@endif">0</span>%</div></div></div>
                            </div>
                            <div class="donate-bar-info">
                                <div class="donate-percent"></div>
                            </div>
                            <div class="amounts clearfix">
                                @if(isset($project->raised_amount))
                                <div class="pull-left number">Raised: tk {{$project->raised_amount}}</div>
                                <div class="pull-right number">Goal: tk {{$project->goal_amount}}
                                 @endif
                            </div>
                        </div>
                        <div class="btns-box">
                            <a href="{{url('/donation/create')}}" target="_blank" class="theme-btn btn-style-five">Donate Now</a>
                            <a href="about.html" class="theme-btn btn-style-two">More Detail</a>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </section>

    <!-- Gallery Section -->
    <section class="gallery-section">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title centered">
                <h2><span class="theme_color">Our Gallery</span>  Projects</h2>
                <div class="text">We have successfully for Poor Peoples <br> completed 500+ projects</div>
            </div>

            <!--MixitUp Galery-->
            <div class="mixitup-gallery">

                <!--Filter-->
                <div class="filters clearfix">

                    <ul class="filter-tabs filter-btns text-center clearfix">
                        <li class="active filter" data-role="button" data-filter="all">All</li>
                        {{--<li class="filter" data-role="button" data-filter=".homeless">Ict Traning</li>--}}
                        {{--<li class="filter" data-role="button" data-filter=".charity">Education</li>--}}
                        {{--<li class="filter" data-role="button" data-filter=".food">Jakat Foundation</li>--}}
                        @foreach($events as $event)
                        <li class="filter" data-role="button" data-filter="{{'.'.$event->class}}">{{$event->project_name}}</li>
                        @endforeach
                    </ul>

                </div>

                <div class="filter-list row clearfix">

                    <!-- Gallery Block -->

                    @foreach($events as $event)
                        <div class="gallery-block mix water {{$event->class}} col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="image">
                                <img src="ui/frontend/images/gallery/1.jpg" alt="" />
                                <div class="overlay-box">
                                    <div class="overlay-inner">
                                        <h3><a href="#">{{$event->title}}</a></h3>
                                        <div class="text">{{$event->detail}}</div>
                                        <ul class="links">
                                            <li><a href="{{$event->facebook_link}}" target="_blank" class="icon flaticon-web-link"></a></li>
                                            <li><a href="ui/frontend/images/gallery/1.jpg" data-fancybox="gallery" data-caption="" class="icon flaticon-full-screen"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach

                </div>
            </div>

            <div class="load-more-btn text-center">
                <a href="#" class="theme-btn btn-style-five">load more</a>
            </div>

        </div>
    </section>
    <!-- End Gallery Section -->

    <!-- Call To Action Section Three -->
    <section class="call-to-action-section-three" style="background-image: url(ui/frontend/images/background/6.jpg)">
        <div class="auto-container">
            <div class="title">Want to join with us</div>
            <h2>BECOME A PROUD VOLUNTEER</h2>
            <div class="text">When you bring together those who have, with those who have not - miracles happen. <br> Become a time hero by volunteering with us. Meet new friends, gain new skills, get <br> happiness and have fun!</div>
            <a href="{{url('/volundeer-signup')}}" target="_blank" class="theme-btn btn-style-five">Join With Us</a>
        </div>
    </section>
    <!-- Call To Action Section Three -->

    <!-- Team Section -->
    <section class="team-section">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title centered">
                <h2><span class="theme_color">What Says Our</span> Donner</h2>
                <div class="text">We have successfully for Poor Peoples <br> completed 500+ projects</div>
            </div>

            <div class="team-carousel owl-carousel owl-theme">

                <!-- Team Block -->
                <div class="team-block">
                    <div class="inner-box">
                        <div class="clearfix">
                            <!-- Image Column -->
                            <div class="image-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <div class="image">
                                        <a href="team.html"><img src="ui/backend/images/team-1.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Content Column -->
                            <div class="content-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <h3><a href="team.html">Jannat Fardause</a></h3>
                                    <div class="location">From USA</div>
                                    <div class="text">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam sit aspernatur aut odit.</div>
                                    <div class="quote-icon flaticon-right-quote-sign"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Team Block -->
                <div class="team-block">
                    <div class="inner-box">
                        <div class="clearfix">
                            <!-- Image Column -->
                            <div class="image-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <div class="image">
                                        <a href="team.html"><img src="ui/backend/images/team-2.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Content Column -->
                            <div class="content-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <h3><a href="team.html">Jone Anithan</a></h3>
                                    <div class="location">From USA</div>
                                    <div class="text">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam sit aspernatur aut odit.</div>
                                    <div class="quote-icon flaticon-right-quote-sign"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Team Block -->
                <div class="team-block">
                    <div class="inner-box">
                        <div class="clearfix">
                            <!-- Image Column -->
                            <div class="image-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <div class="image">
                                        <a href="team.html"><img src="ui/backend/images/team-1.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Content Column -->
                            <div class="content-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <h3><a href="team.html">Jannat Fardause</a></h3>
                                    <div class="location">From USA</div>
                                    <div class="text">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam sit aspernatur aut odit.</div>
                                    <div class="quote-icon flaticon-right-quote-sign"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Team Block -->
                <div class="team-block">
                    <div class="inner-box">
                        <div class="clearfix">
                            <!-- Image Column -->
                            <div class="image-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <div class="image">
                                        <a href="team.html"><img src="ui/backend/images/team-2.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Content Column -->
                            <div class="content-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <h3><a href="team.html">Jone Anithan</a></h3>
                                    <div class="location">From USA</div>
                                    <div class="text">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam sit aspernatur aut odit.</div>
                                    <div class="quote-icon flaticon-right-quote-sign"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Team Block -->
                <div class="team-block">
                    <div class="inner-box">
                        <div class="clearfix">
                            <!-- Image Column -->
                            <div class="image-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <div class="image">
                                        <a href="team.html"><img src="ui/backend/images/team-1.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Content Column -->
                            <div class="content-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <h3><a href="team.html">Jannat Fardause</a></h3>
                                    <div class="location">From USA</div>
                                    <div class="text">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam sit aspernatur aut odit.</div>
                                    <div class="quote-icon flaticon-right-quote-sign"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Team Block -->
                <div class="team-block">
                    <div class="inner-box">
                        <div class="clearfix">
                            <!-- Image Column -->
                            <div class="image-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <div class="image">
                                        <a href="team.html"><img src="ui/backend/images/team-2.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                            <!-- Content Column -->
                            <div class="content-column col-lg-6 col-md-6 col-sm-12">
                                <div class="inner-column">
                                    <h3><a href="team.html">Jone Anithan</a></h3>
                                    <div class="location">From USA</div>
                                    <div class="text">Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam sit aspernatur aut odit.</div>
                                    <div class="quote-icon flaticon-right-quote-sign"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Team Section -->

    <!-- News Section -->
    <section class="news-section-two" style="background-image: url(ui/frontend/images/background/7.jpg)">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title light">
                <h2><span class="theme_color">Our Latest </span> News</h2>
            </div>
            <div class="news-carousel owl-carousel owl-theme">

                <!-- News Block Three -->

                @foreach($events as $event)

                <div class="news-block-three">
                    <div class="inner-box">
                        <div class="image">
                            <a href="blog-single.html"><img src="ui/backend/images/news-3.jpg" alt="" /></a>
                            <div class="read-more"><a href="blog-single.html" class="more">Read More</a></div>
                        </div>
                        <div class="lower-content">
                            <div class="content">
                                <div class="date-outer">
                                    <div class="date">25</div>
                                    {{--<div class="month">jan</div>--}}
                                </div>
                                <h3><a href="{{url('/event/'.$event->id)}}" target="_blank">{{$event->title}}</a></h3>
                                <ul class="post-meta">
                                    <li><a href="{{url('/event/'.$event->id)}}" target="_blank"><span class="icon far fa-folder-open"></span>{{$event->project_name}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!-- End News Section -->

    <!-- Contact Section -->
    <section class="contact-section" style="background-image: url(ui/frontend/images/background/map-pattern-1.png)">
        <div class="auto-container">
            <!-- Sec Title -->
            <div class="sec-title centered">
                <h2><span class="theme_color">Contact </span> Us</h2>
                <div class="text">Thank you very much for your interest in our company and our services </div>
            </div>
            <div class="row clearfix">

                <!-- Column -->
                <div class="info-column col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-column wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-location"></span>
                        </div>
                        <h3>Address:</h3>
                        <div class="text">185, Pickton Near Street, Los <br> Angeles, USA</div>
                    </div>
                </div>

                <!-- Column -->
                <div class="info-column col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-column wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-call"></span>
                        </div>
                        <h3>Phone:</h3>
                        <div class="text">(062) 803-1535, (062) 803-1534,<br> (762) 803-15 35</div>
                    </div>
                </div>

                <!-- Column -->
                <div class="info-column col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-column wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                        <div class="icon-box">
                            <span class="icon flaticon-email-1"></span>
                        </div>
                        <h3>Email:</h3>
                        <div class="text">Info@companyname.com <br> otheremail@gmail.com</div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- End Contact Section -->

    <!--Clients Section-->
    <section class="clients-section">
        <div class="outer-container">

            <div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    @foreach($sponsors as $sponsor)
                    <li class="slide-item"><figure class="image-box"><a href="#"><img src="{{asset('/ui/backend/images/'.$sponsor)}}" alt=""></a></figure></li>
                     @endforeach
                    {{--<li class="slide-item"><figure class="image-box"><a href="#"><img src="ui/frontend/images/clients/2.png" alt=""></a></figure></li>--}}
                    {{--<li class="slide-item"><figure class="image-box"><a href="#"><img src="ui/frontend/images/clients/3.png" alt=""></a></figure></li>--}}
                    {{--<li class="slide-item"><figure class="image-box"><a href="#"><img src="ui/frontend/images/clients/4.png" alt=""></a></figure></li>--}}
                    {{--<li class="slide-item"><figure class="image-box"><a href="#"><img src="ui/frontend/images/clients/5.png" alt=""></a></figure></li>--}}
                    {{--<li class="slide-item"><figure class="image-box"><a href="#"><img src="ui/frontend/images/clients/1.png" alt=""></a></figure></li>--}}
                    {{--<li class="slide-item"><figure class="image-box"><a href="#"><img src="ui/frontend/images/clients/2.png" alt=""></a></figure></li>--}}
                </ul>
            </div>

        </div>
    </section>
    <!--End Clients Section-->


@endsection
