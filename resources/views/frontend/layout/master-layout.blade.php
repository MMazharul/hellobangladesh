<!DOCTYPE html>
<html>

<!-- Mirrored from expert-themes.com/html/khidmat/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 27 Jun 2019 04:03:40 GMT -->
<head>
    <meta charset="utf-8">
    <title>Hello | Bangladesh</title>
    <!-- Stylesheets -->
    <link href="{{asset('ui/frontend/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('ui/frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('ui/frontend/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('ui/frontend/css/responsive.css')}}" rel="stylesheet">
    <!--Color Switcher Mockup-->
    <link href="{{asset('/css/color-switcher-design.css')}}" rel="stylesheet">
    <!--Color Themes-->
    <link id="theme-color-file" href="{{asset('ui/frontend/css/color-themes/default-theme.css')}}" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('/ui/frontend/ui/frontend/images/favicon.png')}}" type="image/x-icon">
    {{--<link rel="icon" href="{{asset('/')}}ui/frontend/images/favicon.png" type="image/x-icon">--}}

    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="{{asset('ui/frontend/js/respond.js')}}"></script><![endif]-->
</head>

<body class="hidden-bar-wrapper">

<div class="page-wrapper">

    <!-- Preloader -->
{{--<div class="preloader"></div>--}}

<!-- Main Header / Header Style Two -->
   @include('frontend.layout.header')
    <!--End Main Header -->

    <!--Main Slider-->

    @yield('main_content')

    <!--Main Footer-->

    @include('frontend.layout.footer')


</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>


<script src="{{asset('/ui/frontend/')}}/js/jquery.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/popper.min.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/bootstrap.min.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/jquery.fancybox.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/appear.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/owl.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/wow.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/mixitup.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/slick.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/nav-tool.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/jquery.magnific-popup.min.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/main.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/jquery-ui.js"></script>
<script src="{{asset('/ui/frontend/')}}/js/script.js"></script>
{{--<script src="{{asset('/ui/frontend/')}}/js/color-settings.js"></script>--}}

</body>

<!-- Mirrored from expert-themes.com/html/khidmat/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 27 Jun 2019 04:03:48 GMT -->
</html>

