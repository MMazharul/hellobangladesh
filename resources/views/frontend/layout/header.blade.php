<header class="main-header header-style-two">

    <!-- Header Top Two -->
    <div class="header-top-two">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Top Left-->
                <div class="top-left col-lg-7 col-md-12 col-sm-12">
                    <ul>
                        <li>Welcome to the Hello | Bangladesh</li>
                        <li><span>You have any question?</span> <a href="tel:+440-98-5298">+440-98-5298</a></li>
                    </ul>
                </div>

                <!--Top Right-->
                <div class="top-right col-lg-5 col-md-12 col-sm-12">
                    <a href="{{url('/donation/')}}" class="theme-btn donate-btn">Donate Now</a>
                    <!--Social Box-->
                    <ul class="social-box">
                        <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                        <li><a href="#"><span class="fab fa-google-plus-g"></span></a></li>
                        <li><a href="#"><span class="fab fa-skype"></span></a></li>
                        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fab fa-linkedin-in"></span></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <!--Header-Upper-->
    <div class="header-upper">
        <div class="auto-container">
            <div class="clearfix">

                <div class="pull-left logo-box">
                    <div class="logo"><a href="index-2.html"><img src="ui/frontend/images/logo-2.png" alt="" title=""></a></div>
                </div>

                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="#">Home</a></li>
                                <li class=""><a href="#">About Us</a></li>
                                <li class=""><a href="#mission">Donate</a></li>
                                <li class=""><a href="#">Program</a></li>
                                <li class=""><a href="#">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>

                    <!-- Main Menu End-->
                    <div class="outer-box clearfix">
                        <div class="btn-box">
                            <!--Search Btn-->
                            <div href="#modal-popup-2" class="navsearch-button xs-modal-popup"><i class="icon flaticon-magnifying-glass-1"></i></div>

                            <!-- Main Menu End-->
                            <div class="nav-box">
                                <div class="nav-btn nav-toggler navSidebar-button"><span class="icon flaticon-menu-1"></span></div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
    <!--End Header Upper-->

    <!--Sticky Header-->
    <div class="sticky-header">
        <div class="auto-container clearfix">
            <!--Logo-->
            <div class="logo pull-left">
                <a href="index-2.html" class="img-responsive"><img src="ui/frontend/images/logo-small.png" alt="" title=""></a>
            </div>

            <!--Right Col-->
            <div class="right-col pull-right">
                <!-- Main Menu -->
                <nav class="main-menu navbar-expand-md">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1">
                        <ul class="navigation clearfix">
                            <li class=""><a href="#">Home</a></li>
                            <li class=""><a href="#">About Us</a></li>
                            <li class=""><a href="">Donate</a></li>
                            <li class=""><a href="#">Program</a></li>
                            <li><a href="">Contact us</a></li>
                        </ul>
                    </div>
                </nav><!-- Main Menu End-->
            </div>

        </div>
    </div>
    <!--End Sticky Header-->

</header>