<footer class="main-footer style-two" style="background-image:url(ui/frontend/images/background/8.jpg)">
    <div class="auto-container">
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row clearfix">

                <!-- Big Column -->
                <div class="big-column col-lg-6 col-md-12 col-sm-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-lg-7 col-md-6 col-sm-12">
                            <div class="footer-widget logo-widget">
                                <div class="logo">
                                    <a href="index-2.html"><img src="ui/frontend/images/footer-logo.png" alt="" /></a>
                                </div>
                                <div class="text">Core values are the fundamental beliefs of a person or organization. The core values are the guiding prin ples that dictate behavior and action suas labore saperet has there any quote for write lorem percit latineu.</div>
                                <!--Social Box-->
                                <ul class="social-box">
                                    <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fab fa-google-plus-g"></span></a></li>
                                    <li><a href="#"><span class="fab fa-skype"></span></a></li>
                                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fab fa-linkedin-in"></span></a></li>
                                </ul>
                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-lg-5 col-md-6 col-sm-12">
                            <div class="footer-widget links-widget">
                                <h2>Our Projects</h2>
                                <ul class="footer-list">
                                    <li><a href="#">Water Surve</a></li>
                                    <li><a href="#">Eduction For All</a></li>
                                    <li><a href="#">Treatment</a></li>
                                    <li><a href="#">Food Surve</a></li>
                                    <li><a href="#">Cloth Surve</a></li>
                                    <li><a href="#">Selter Project</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Big Column -->
                <div class="big-column col-lg-6 col-md-12 col-sm-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-lg-7 col-md-6 col-sm-12">
                            <div class="footer-widget news-widget">
                                <h2>Recent Posts</h2>

                                <article class="post">
                                    <figure class="post-thumb"><a href="blog-single.html"><img src="ui/frontend/images/resource/news-thumb-1.jpg" alt=""></a></figure>
                                    <div class="text"><a href="blog-single.html">Feeding for the poor people help these life.</a></div>
                                    <div class="post-info">12 Dec, 2018</div>
                                </article>

                                <article class="post">
                                    <figure class="post-thumb"><a href="blog-single.html"><img src="ui/frontend/images/resource/news-thumb-2.jpg" alt=""></a></figure>
                                    <div class="text"><a href="blog-single.html">Take care of children for the greate serving</a></div>
                                    <div class="post-info">12 Dec, 2018</div>
                                </article>

                            </div>
                        </div>

                        <!--Footer Column-->
                        <div class="footer-column col-lg-5 col-md-6 col-sm-12">
                            <div class="footer-widget links-widget">
                                <h2>Quick Links</h2>
                                <ul class="footer-list">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Donation Form</a></li>
                                    <li><a href="#">Projects</a></li>
                                    <li><a href="#">Events</a></li>
                                    <li><a href="#">Coseses</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Footer Bottom -->
    <div class="footer-bottom-two">
        <div class="auto-container">
            <div class="clearfix">
                <div class="pull-left">
                    <div class="copyright">Copyrights © 2019 <a href="">Hello | Bangladesh.</a> All rights reserved.</div>
                </div>
                <div class="pull-right">
                    <ul class="footer-nav">
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Sitemap</a></li>
                        <li><a href="#">Terms of Use</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</footer>