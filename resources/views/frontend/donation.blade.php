@extends('frontend.layout.master-layout')
@section('main_content')
    <section class="donate-form-section" style="background-image:url({{asset('ui/frontend/images/background/10.jpg')}})">
        <div class="auto-container">
            <div class="row clearfix">

                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="image">
                            <img src="{{asset('/ui/frontend/images/resource/image-2.png')}}" alt="" />
                        </div>
                    </div>
                </div>

                <!-- Form Column -->
                <div class="form-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">

                        @if(session('msg'))
                            <div class="alert alert-success alert-dismissible" role="alert" id="message">
                                <div class="icon"><span class="mdi mdi-check"></span></div>
                                <div class="message text-center "><h6>{{ session('msg') }}</h6></div>
                            </div>
                        @endif

                        <!-- Sec Title -->
                        <div class="sec-title light centered">
                            <h2><span class="theme_color">Donate </span> Now</h2>
                            <div class="text">We are always looking out and timely help disadvantaged, <br> See our latest campaign and if can you pledonaa</div>
                        </div>

                        <!-- Default Form -->
                        <div class="default-form style-two">
                            <form method="POST" action="{{url('/donation/store/')}}">
                                @csrf

                                <div class="row clearfix">

                                    <!-- Form Group -->
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">

                                        <select name="project">
                                            <option >Project You Want to donation</option>
                                            @foreach($projects as $key=>$project)
                                                <option value="{{$key}}">{{$project}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                        <input type="text" name="firstname" value="" placeholder="First Name" required>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                        <input type="text" name="lastname" value="" placeholder="Last Name" required>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <input type="text" name="amount" value="" placeholder="Your Gift ( Taka )" required>
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <input type="text" name="transaction" value="" placeholder="Transaction ID" required>
                                    </div>

                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <input type="text" name="mobile" value="" placeholder="Contact Number" required>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                        <textarea name="message" placeholder="Message"></textarea>
                                    </div>

                                    <div class="form-group text-center col-lg-12 col-md-12 col-sm-12">
                                        <button type="submit" class="btn-style-three"><span class="txt">Fund now</span></button>
                                    </div>

                                </div>

                            </form>
                        </div>
                        <!--End Default Form-->

                    </div>
                </div>

            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>
            jQuery(
                function($) {
                    $('#message').fadeIn (5050);
                    $('#message').fadeOut (5050);

                }
            )

        </script>

    </section>
@endsection