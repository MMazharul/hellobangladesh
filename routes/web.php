<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');


Route::get('/admin', function () {
    return view('admin/layouts/master');
});


Route::get('/volundeer-signup','VolunteerController@create');

Route::get('/donation/create', 'DonationController@create');



Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function() {
    Route::get('/admin', 'HomeController@admin');
    Route::resource('/event','EventController');
    Route::get('/event/{id}/delete','EventController@delete');
    Route::get('/slider/{id}/delete','SliderController@delete');
    Route::resource('/gallery', 'GalleryController');
    Route::resource('/slider', 'SliderController');
    Route::resource('/volunteer', 'VolunteerController');
    Route::resource('/project', 'ProjectController');
    Route::resource('/sponsor', 'SponsorController');
    Route::post('/donation/store/', 'DonationController@store');
    Route::get('/donation/', 'DonationController@index');


});
