<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $fillable=['project','firstname','lastname','mobile','amount','transaction','message'];
}
