<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable=['title','picture','short_description','long_description','raised_amount','goal_amount','status'];
}
