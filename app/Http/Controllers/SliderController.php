<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\Facades\Image;



class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders=Slider::all();
        return view('admin/slider/index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/slider/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $data=$request->only('title','short_description','picture');

            if($request->hasFile('picture')) {

                $imgeFile = $request['picture'];
                $imgFileName = time() . '-' . $imgeFile->getClientOriginalName();

                $newExtension="png";
                $new_extensionPicture= substr($imgFileName, 0, -strlen(pathinfo($imgFileName, PATHINFO_EXTENSION))).$newExtension;
                $data['picture']=$new_extensionPicture;
                $directory = public_path('/ui/backend/images/');
                $imgUrl = $directory.$new_extensionPicture;
                Image::make($imgeFile)->resize(1000, 500)->save($imgUrl);
            }
            else{
                $data['picture'] = 0;
            }
            Slider::create($data);

            return redirect('/slider');


        } catch(QueryException $e){
            return redirect()
                ->route('slider.create')
                ->withInput()
                ->withErrors($e->getMessage());
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {

        return view('admin/slider/edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $imgExit=Slider::find($id);

        $data=$request->all();

        if($request->picture==null)
        {
              $data['picture']=$imgExit->picture;

        }

        else{
            if(file_exists(public_path('/ui/backend/images/'.$imgExit->picture))){

                unlink(public_path('/ui/backend/images/'.$imgExit->picture));


            }
            $imgeFile = $request['picture'];
            $imgFileName = time().'-'.$request->picture->getClientOriginalName();
//
            $newExtension="png";
            $new_extensionPicture= substr($imgFileName, 0, -strlen(pathinfo($imgFileName, PATHINFO_EXTENSION))).$newExtension;
            $data['picture']=$new_extensionPicture;
            $directory = public_path('/ui/backend/images/');
            $imgUrl = $directory.$new_extensionPicture;
            Image::make($imgeFile)->resize(1000,500)->save($imgUrl);

        }

        $imgExit->update($data);

        return redirect('/slider')->with('message', 'Slider Update Successfully !');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
         $slider=Slider::findOrfail($id);
        $imgLocation=public_path('ui/backend/images/'.$slider->picture);

         if(File::isDirectory($imgLocation))
         {
             unlink($imgLocation);
         }

         $slider->delete();
        return redirect('/slider');

    }
}
