<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use Illuminate\Database\QueryException;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects=Project::all();
        return view('admin/project/index',compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/project/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $data=$request->all();
            if($request->hasFile('picture')) {
                $imgeFile = $request['picture'];
                $imgFileName= time().'-'.$imgeFile->getClientOriginalName();
                $directory = public_path('/ui/backend/images/');

                $imgUrl = $directory.$imgFileName;
                Image::make($imgeFile)->resize(1000,500)->save($imgUrl);
                $data['picture']=$imgFileName;
            }
            else
            {
                $data['picture']=0;
            }

            Project::create($data);
            return redirect('/project');

        }catch(QueryException $e){
            return redirect('project/create')
                ->withInput()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('admin/project/edit',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imgExit=Project::find($id);

        $data=$request->all();

        if($request->picture==null)
        {
            $data['picture']=$imgExit->picture;

        }

        else{
            if(file_exists(public_path('/ui/backend/images/'.$imgExit->picture))){

                unlink(public_path('/ui/backend/images/'.$imgExit->picture));


            }
            $imgeFile = $request['picture'];
            $imgFileName = $request->title.'-'.$request->picture->getClientOriginalName();

            $data['picture']=$imgFileName;
            $directory = public_path('/ui/backend/images/');
//
//            $newExtension=".png";
//            $new_extensionPicture= substr($imgFileName, 0, -strlen(pathinfo($imgFileName, PATHINFO_EXTENSION))).$newExtension;

            $imgUrl = $directory.$imgFileName;
            Image::make($imgeFile)->resize(1000,500)->save($imgUrl);

        }

        $imgExit->update($data);

        return redirect('/project')->with('message', 'project Update Successfully !');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project=Project::findOrfail($id);
        $imgLocation=public_path('ui/backend/images/'.$project->picture);

        if(File::isDirectory($imgLocation))
        {
            unlink($imgLocation);
        }

        $project->delete();
        return redirect('/project');
    }
}
