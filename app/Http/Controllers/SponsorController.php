<?php

namespace App\Http\Controllers;

use App\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
class SponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sponsors=Sponsor::all();
        return view('/admin/sponsor/index',compact('sponsors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/sponsor/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

            $data=$request->only('title','picture');

            if($request->hasFile('picture')) {

                $imgeFile = $request['picture'];
                $imgFileName = time() . '-' . $imgeFile->getClientOriginalName();

//                $newExtension="png";
//                $new_extensionPicture= substr($imgFileName, 0, -strlen(pathinfo($imgFileName, PATHINFO_EXTENSION))).$newExtension;
                $data['picture']=$imgFileName;
                $directory = public_path('/ui/backend/images/');
                $imgUrl = $directory.$imgFileName;
                Image::make($imgeFile)->resize(250, 100)->save($imgUrl);
            }
            else{
                $data['picture'] = 0;
            }
            Sponsor::create($data);

            return redirect('/sponsor');


        } catch(QueryException $e){
            return redirect()
                ->route('sponsor.create')
                ->withInput()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sponsor=Sponsor::findOrfail($id);
        $imgLocation=public_path('ui/backend/images/'.$sponsor->picture);

        if(File::isDirectory($imgLocation))
        {
            unlink($imgLocation);
        }

        $sponsor->delete();
        return redirect('/sponsor');
    }
}
