<?php

namespace App\Http\Controllers;

use App\Event;
use App\Project;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;

use Intervention\Image\Facades\Image;


class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events=Event::all();
        return view('/admin/event/index',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects=Project::pluck('title');

        return view('/admin/event/create',compact('projects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $convertClassname=str_replace(array(' ',';',':','\'','(',')','[',']'),'_', $request->project_name);

            $data['class']=$convertClassname;


            if($request->hasFile('picture')) {

                $imgeFile = $request['picture'];
                $imgFileName = time() . '-' . $imgeFile->getClientOriginalName();
                $directory = public_path('/ui/backend/images/');
                $imgUrl = $directory . $imgFileName;
                Image::make($imgeFile)->resize(1000, 500)->save($imgUrl);
                $data['picture'] = $imgFileName;
            }
            else{
                $data['picture'] = 0;
            }

            Event::create($data);
            return redirect('/event')->withMessage('Store Successfully!!!');
        }
        catch (\Illuminate\Database\QueryException $e) {

            return redirect('/event/create')
                ->withInput()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event=Event::findOrfail($id);
        return view('/frontend/single-event-page',compact($event));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('/admin/event/edit',compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $event->update($request->all());
        return redirect('/event')->with('Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
         Event::findorfail($id)->delete();
        return redirect('/event')->with('Success');
    }
}
