<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $guestid=null;


    public function __construct()
    {

        if(session()->has('session_id'))
        {

            $this->guestid=session()->put('session_id',time().'-'.rand());
        }

        if (is_null($this->guestid)) {
            session()->put('session_id', time().'-'.rand());
        }
        $this->guestid= Session::get('session_id');



        $id=$this->guestid;

        \View::share('id',$id);
    }



}
