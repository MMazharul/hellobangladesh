<?php

namespace App\Http\Controllers;

use App\Artical;
use App\Event;
use App\Project;
use App\Slider;
use App\Sponsor;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $sliders=Slider::latest()->get();
//        $articals=Artical::latest()->get();
       $projects=Event::pluck('title','id');
       $events=Event::latest()->get();
        $project=Project::where('status',1)->first();
        $sponsors=Sponsor::pluck('picture');

        return view('/frontend/index',compact('sliders','project','events','sponsors'));
    }
}
