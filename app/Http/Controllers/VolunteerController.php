<?php

namespace App\Http\Controllers;

use App\Volunteer;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;

use Intervention\Image\Facades\Image;

class VolunteerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $volunteers=Volunteer::latest()->get();
        return view('/admin/volunteer/index',compact('volunteers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/frontend/volundeer-signup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->all();

        $imgeFile = $request['picture'];
        $imgFileName= time().'-'.$imgeFile->getClientOriginalName();
        $directory = public_path('/ui/backend/images/');
        $imgUrl = $directory.$imgFileName;
        Image::make($imgeFile)->resize(100,50)->save($imgUrl);
        $data['picture']=$imgFileName;

        Volunteer::create($data);

        return redirect('/volundeer-signup')->with('msg','Thank You for Membership in Hello Bangladesh');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $volunteer=Volunteer::findOrfail($id);
        $imgLocation=public_path('/ui/backend/images/'.$volunteer->picture);

        if(File::isDirectory($imgLocation))
        {
            unlink($imgLocation);
        }

        $volunteer->delete();
        return redirect('/volunteer');
    }
}
