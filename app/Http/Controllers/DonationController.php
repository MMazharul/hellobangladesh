<?php

namespace App\Http\Controllers;

use App\Donation;
use App\Event;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class DonationController extends Controller
{
    function store(Request $request)
    {
        $data=$request->all();

        $project=Project::findorfail($request->project);
        $project->raised_amount+=$request->amount;
        $project->save();
        Donation::create($data);

        return Redirect::back()->with('msg','Thank You Your Donate Submit Success');

    }

    function index()
    {
        $donations=Donation::latest()->get();
        return view('admin/donation/index',compact('donations'));
    }

    public function create()
    {
        $projects=Project::latest()->pluck('title','id');

        return view('/frontend/donation',compact('projects'));
    }
}
