<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    protected $fillable=['first_name','last_name','gender','date_of_birth','address','phone','phone','blood_group','picture','profession'];
}
