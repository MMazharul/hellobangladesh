<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable=['class','project_name','title','facebook_link','picture','detail','date','location','status'];
}
